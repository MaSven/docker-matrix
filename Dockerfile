FROM debian:stretch-slim

# Maintainer
MAINTAINER Sven Marquardt

# install homerserver template
COPY adds/start.sh /start.sh

# add supervisor configs
COPY adds/supervisord-matrix.conf /conf/
COPY adds/supervisord-turnserver.conf /conf/
COPY adds/supervisord.conf /
COPY conf/*.* /data/

# startup configuration
ENTRYPOINT ["/start.sh"]
CMD ["start"]
EXPOSE 8448

# Git branch to build from
ARG BV_SYN=master
ARG BV_TUR=master
ARG TAG_SYN=v0.31.1

# user configuration
ENV MATRIX_UID=991 MATRIX_GID=991

# use --build-arg REBUILD=$(date) to invalidate the cache and upgrade all
# packages
ARG REBUILD=1
RUN mkdir /uploads 
RUN export DEBIAN_FRONTEND=noninteractive
RUN mkdir -p /var/cache/apt/archives
RUN touch /var/cache/apt/archives/lock
RUN apt-get clean
RUN apt-get update -y -q --fix-missing \
	&& apt-get upgrade -y \
	&& apt-get install -y --no-install-recommends \
        build-essential python2.7-dev libffi-dev \
                     python-pip python-setuptools sqlite3 \
                     libssl-dev python-virtualenv libjpeg-dev libxslt1-dev \
	&& apt-get upgrade -y
RUN pip install --upgrade virtualenv
RUN virtualenv -p python2.7 /synapse
ENV PATH /synapse/bin/activate:$PATH
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install --upgrade supervisor
RUN pip install https://github.com/matrix-org/synapse/tarball/master
RUN cd /synapse
ENV PATH /synapse/bin/activate:$PATH
RUN apt-get autoremove -y 
RUN rm -rf /var/lib/apt/* /var/cache/apt/*
RUN cat /data/homeserver.yaml

    