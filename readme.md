# Docker image for Matrix

- [Matrix Chat](https://matrix.aventer.biz/#/room/#dockermatrix:matrix.aventer.biz)
- [Docker Hub](https://hub.docker.com/r/avhost/docker-matrix/tags)
- [Donation](https://liberapay.com/docker-matrix/)

## Notice

This docker instance is only for testing purpose. All settings are done so that login and getting info is easiest for apis to verify correct behaviour. It is not meant for production use.

## Admin User
To create an admin user the api endpoint `/_matrix/client/api/v1/register` can be used with an POST request. The json data should be look like this:
  
    {
    	"user":"user",
		"password":"password",
		"mac":"hmac",
		"type":"org.matrix.login.shared_secret",
		"admin":true
    }
Where as  
 
* user stores the user name
* password is the password of the user
* mac is a hmac as described here [rf2104](https://tools.ietf.org/html/rfc2104.html) of all the content. Example in python here: [Example](https://github.com/matrix-org/synapse/blob/667c6546bdbb50c1b5a88effe02ae12322d0d95b/scripts/register_new_matrix_user#L40)
* type is the type of how the user will register. This must be org.matrix.login.shared_secret otherwise the register will not function
* admin if this user is an admin. This must be true for the admin user

## Shared secret
The shared secrete used by this instance is **OVrpY6216E.F*DUJ1pVZERKgeK6.zr1WddMUlUJ,TdSlfRTi26** only if you use the config of this docker instance.